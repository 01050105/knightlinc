# knightlinc

Include files maintained by Knightly.  Possibly only useful to Knightly.

## Write

Write is a configurable logging library useful for printing out messages.  It allows you to set different logging levels and control how the output of those logging levels is handled.

### Write Usage

A very simple example of usage is:

```lua
-- Require the library
local Write = require('knightlinc/Write')
-- Add a prefix to identify which location messages are being logged from
Write.prefix = 'MyCoolScript'
-- Print an informational message
Write.Info('Hello World!')
```

In this scenario, only logs above 'info' would be written out.  That means that trace and debug messages will not show, while info, warn, error, fatal, and help messages will show.

In this way you can flip between adding debug messages and whether those messages show or not. By just toggling Write.loglevel to 'debug':

```lua
local Write = require('knightlinc/Write')
Write.prefix = 'MyCoolScript'
Write.Debug('This message will not show.')
Write.loglevel = 'debug'
Write.Debug('This message will show.')
```

Lastly, write handles string formatting for you, so you can use it just like you would string.format:

```lua
local Write = require('knightlinc/Write')
Write.prefix = 'MyCoolScript'
Write.Info('This is script (%s) which is written by %s', Write.prefix, 'ACoolPerson')
```

### Default logging levels

`Write.Trace` - Useful for stepping through each bit of code and almost never shown to the end-user.

`Write.Debug` - Debug messages.  Often items that you might have an end-user turn on to help with debugging.

`Write.Info` - Standard level of messages.  Normal output.

`Write.Warn` - Warning messages.  Something might be wrong.

`Write.Error` - Error messages.  Something is wrong.

`Write.Fatal` - Fatal error messages, execution will stop after this message is displayed.

`Write.Help` - Help messages.  Usually usage instructions.

### Write Configuration Options

`Write.usecolors` - `bool` - Whether colors should be on or off.  On by default.

`Write.loglevel` - `string` - The log level at or above which messages start being printed to the console.  Default is 'info'.

`Write.prefix` - `string` or `function` that returns `string` - This will appear at the very beginning of the line.  The function portion is useful for inserting something like a timestamp.  However, the script's name as a string will also suffice and is how it is most often used.  Default is empty.

`Write.postfix` - `string` or `function` that returns `string` - This will appear at the end of the write string, prior to the separator (which itself is just before the log entry displays).  Default is empty.

`Write.separator` - `string` - This is the notation that appears in between the write string and the log entry to be printed.  Default is `' :: '`

`Write.callstringlevel` - `number` - The log level at or below which messages will include the script they came from and their line numbers.  Default is Debug (`Write.loglevels['debug'].level`)

#### Loglevel Configuration Options

Loglevels themselves can be configured as well.  The properties for these are `Write.loglevels['loglevel'].<property>`  For example, to set the MQ color of trace, you can do: `Write.loglevels['trace'].mqcolor = '\at'`

Properties are:

`level` - `number` - Used for ordering which log levels are "above" or "below" others.

`color` - `string` - The ansi color code for console output

`mqcolor` - `string` - The macroquest color code for chat output

`abbreviation` - `string` - How a particular log level will be abbreviated

`terminate` - `boolean` - Whether to call the Terminate() (end program) function when this log level is hit

#### Adding or removing log levels

An example of adding and removing a custom log level is below.  Note that the loglevels only support lower case and handle changing to sentence case for calls on their own.

Example:

```lua
    -- Add the log level (note custom vs Custom)
    Write.loglevels.custom = {
        level = 8,
        color = '\27[35m',
        mqcolor = '\ap',
        abbreviation = '[CUSTOM]',
        terminate = false
    }

    -- Output at the log level (note Custom vs custom)
    Write.Custom('Test')
    -- Remove the log level
    Write.loglevels.custom = nil
    -- This should show an error message and stop execution
    Write.Custom('Test2')
```

Setting one of the default log levels to nil will also remove it.
